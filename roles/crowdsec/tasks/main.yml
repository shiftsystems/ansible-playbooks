- name: Add GPG key on Debian Based systems
  ansible.builtin.apt_key:
    url: "{{ item }}"
    state: present
  become: true
  when: ansible_facts['os_family'] == 'Debian'
  with_items: "{{ gpg_keys }}"

- name: Add Crowdsec repo
  ansible.builtin.shell: 'curl -s https://install.crowdsec.net | sh'
  become: true

- name: Install Crowdsec packages
  ansible.builtin.package:
    name: "{{ packages }}"
    state: latest
  become: true

- name: Start and enable Crowsdsec
  ansible.builtin.systemd:
    name: crowdsec
    state: restarted
    enabled: true
  become: true

- name: Configure Crowdsec firewall bouncer
  block:
  - name: Restart Crowdsec firewall bouncer
    ansible.builtin.systemd:
      name: crowdsec-firewall-bouncer
      state: restarted
      enabled: true
    become: true
  rescue:
  - name: Register API key
    ansible.builtin.shell: "cscli bouncers add local | grep '^[[:space:]]'"
    register: crowdsec_api_key
    become: true

  - name: Add API to firewall config
    ansible.builtin.lineinfile:
      path: /etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml
      regexp: 'api_key:'
      line: 'api_key: {{ crowdsec_api_key.stdout }}'
    become: true

  - name: Restart Crowdsec Firewall Bouncer
    ansible.builtin.systemd:
      name: crowdsec-firewall-bouncer
      state: restarted
      enabled: true
    become: true

- name: Change default port in crowdsec config
  ansible.builtin.lineinfile:
    path: /etc/crowdsec/config.yaml
    regexp: '    listen_uri:'
    line: '    listen_uri: 127.0.0.1:7070'
  become: true

- name: Change default port in crowdsec api config
  ansible.builtin.lineinfile:
    path: /etc/crowdsec/local_api_credentials.yaml
    regexp: 'url:'
    line: 'url: http://127.0.0.1:7070'
  become: true

- name: Change default port in crowdsec firewall bouncer config
  ansible.builtin.lineinfile:
    path: /etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml
    regexp: 'api_url:'
    line: 'api_url: http://localhost:7070/'
  become: true

- name: Restart Crowdsec services
  ansible.builtin.systemd:
    name: '{{ item }}'
    state: restarted
  become: true
  with_items:
    - crowdsec
    - crowdsec-firewall-bouncer

- name: Install Crowdsec Scenarios
  ansible.builtin.shell: "cscli scenarios install {{ item }}"
  become: true
  with_items: "{{ crowdsec_scenarios }}"
  when: crowdsec_scenarios is defined

- name: Install Crowdsec Collections
  ansible.builtin.shell: "cscli collections install {{ item }}"
  become: true
  with_items: "{{ crowdsec_collections }}"
  when: crowdsec_collections is defined

- name: Enroll Crowdsec in the Console
  ansible.builtin.shell: "cscli console enroll -e context {{ crowdsec_console_key }}"
  become: true
  when: crowdsec_console_key is defined
