# Crowdsec
Deploys Crowdsec

Scenarios can be installed by defining a list called `crowdsec_scenarios`
Collections can be installed by defining a list called `crowdsec_collections` container collections from hub.crowdsec.net
Hosts can be enrolled in the console by setting `crowdsec_console_key` to the enroll key listed in your crowdsec console
