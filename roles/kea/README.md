# Kea
Deploys the Kea DCHP server with Kea CTRL agent and stork agent for observability
Requires a valid kea config referenced by the `kea_server_v4_config` path
