# Falco
Falco is an open source security agent that notifies users of security events that happen on their systems such as interactive logins and sensitive files being accessed.
This role configures JSON loging so events can easily by consumed by a SIEM.
The role will also install prevent installation on on LXC hosts since it relies on EBPF or a kernel module