# Auto Updates
Enables DNF Automatic or Unattended upgrades on the system.
the upgrade time is controlled by the `unattended_upgrade_time` for Unattended upgrades which is a 24 hour time, and the dnf_upgrade_times for DNF Automatic which is a list of systemd timer events.
These do not configure email notifications and assumes you have centralized observability to handle notifying you if updates are not working.

